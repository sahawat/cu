package com.example.generator;

import com.github.abrarsyed.jastyle.ASFormatter;
import com.github.abrarsyed.jastyle.FormatterHelper;
import com.github.abrarsyed.jastyle.constants.EnumFormatStyle;
import com.github.abrarsyed.jastyle.constants.SourceMode;
import org.apache.commons.lang3.StringUtils;

import java.io.*;

/**
 * Created by MAY on 25/5/2016.
 */
public class WriteFileUtility {

    private String destinationPath;


    public WriteFileUtility(String destinationPath){
        this.destinationPath = destinationPath;
    }

    public String writeJavaFile(String className,String content){
        try {

            if(StringUtils.isEmpty(content) || StringUtils.isEmpty(className)){
                return "";
            }
            String finalContent = content;

            File file = new File(destinationPath+"\\"+className+".java");
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            //BufferedWriter bw = new BufferedWriter(fw);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getAbsoluteFile()),"UTF-8"));
            System.out.println("finalContent :: "+finalContent);
            bw.write(reformatCode(finalContent));
            bw.close();
            return className;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String reformatCode(String finalCode) throws IOException {
        ASFormatter formatter = new ASFormatter();
        formatter.setSourceStyle(SourceMode.JAVA);
        formatter.setFormattingStyle(EnumFormatStyle.JAVA);
        formatter.setSwitchIndent(true);

        Reader targetReader = new StringReader(finalCode);
        String code = FormatterHelper.format(targetReader,formatter);
        targetReader.close();
        System.out.println("Code :: "+code);
        return code;
    }
}
