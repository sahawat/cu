package com.example.generator;

import com.example.generator.main.model.Attribute;
import com.example.generator.main.model.ClassModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

import static com.example.generator.MainProcess.*;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Attribute;


public class ClassDiagramGenerator {

//    private static List<ClassModel> LIST_OF_CLASSMODEL = new ArrayList<>();
//    private static List<Attribute> LIST_OF_ATTRIBUTE = new ArrayList<>();

    private static Map<String,List<Attribute>> attributeMap = new HashMap<>();



    public Map<String ,List<Attribute>> getAttributeMap(File classDgFile) throws IOException, ParseException {
        //BufferedReader cdJson = new BufferedReader(new FileReader("C:\\Users\\MAY\\Desktop\\ConvertUMLPROJ\\JSON\\CDinput.json"));

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(classDgFile));
        JSONObject jsonObject = (JSONObject) obj;

        jsonObject = (JSONObject) jsonObject.get("xmi:XMI");
        JSONArray model = (JSONArray) jsonObject.get("uml:Model");
        model = getArrayFromJsonArray(model, "ownedMember");
        //System.out.println("model : "+model);

        List<JSONObject> classIden = getObjectsFromJsonArray(model);

        //Class
        classIden.stream().forEach(fragmentChild -> {
            JSONObject cla = getObjectFromJsonObject(fragmentChild, "$");
            ClassModel classTemp = new ClassModel();
            classTemp.setId((String) cla.get("xmi:id"));
            classTemp.setName((String) cla.get("name"));
            classTemp.setVisibility((String) cla.get("visibility"));
            classTemp.setType((String) cla.get("xmi:type"));
            String type = classTemp.getType();
            if(type.equals("uml:Class")){
//                LIST_OF_CLASSMODEL.add(classTemp);
                System.out.println(classTemp.getName());
                JSONArray attrArr = getArrayFromJsonObject(fragmentChild, "ownedAttribute");
                List<JSONObject> attrIden = getObjectsFromJsonArray(attrArr);
                List<Attribute> attributeList = new ArrayList<>();
                if(null != attrIden){
                    Attribute attrTemp;
                    for (JSONObject attrOb : attrIden){
                        attrTemp = new Attribute();
                        JSONObject attrObj = getObjectFromJsonObject(attrOb, "$");
                        attrTemp.setId((String) attrObj.get("xmi:id"));
                        attrTemp.setName((String) attrObj.get("name"));
                        attrTemp.setType(((String) attrObj.get("type")).replace("_id",""));
//                        attrTemp.setTypeUml((String) attrObj.get("xmi:type"));
                        attrTemp.setModifier((String)attrObj.get("visibility"));
                        attributeList.add(attrTemp);
                    }
                }
                attributeMap.put(classTemp.getName(),attributeList);

            }
        });
      return attributeMap;
    }
}