package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by sahawat on 6/5/2016.
 */
@Getter
@Setter
@ToString
public class FragmentLifeLine {

    private String eventId;
    private String lifeLine;
    private String message;
}
