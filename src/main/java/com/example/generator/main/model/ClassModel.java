package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by MAY on 4/7/2016.
 */
@Setter
@Getter
@ToString
public class ClassModel {
    private String id;
    private String name;
    private String type;
    private String visibility;
}
