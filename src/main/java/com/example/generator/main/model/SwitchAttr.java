package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by MAY on 31/5/2016.
 */
@Getter
@Setter
@ToString
public class SwitchAttr {
    private String condition;
    private String returnMessage;
    private String returnAttr;

}
