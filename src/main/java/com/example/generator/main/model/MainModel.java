package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by MAY on 11/5/2016.
 */
@Getter
@Setter
@ToString
public class MainModel {

    private Long id;
    private String name;
    private String receiveEvent;
    private String sendEvent;
    private String covered;

}
