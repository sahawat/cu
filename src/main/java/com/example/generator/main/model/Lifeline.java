package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * Created by MAY on 30/5/2016.
 */
@Getter
@Setter
@ToString
public class Lifeline {
    private String id;
    private String type;
    private String name;
    private JSONArray coveredBy;
    private List<String> listOfEvent;
}