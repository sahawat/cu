package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by sahawat on 6/28/2016.
 */
@Getter
@Setter
@ToString
public class ProcessResult {

    private int classSum;
    private int methodSum;
    private int conditionSum;
    private List<String> fileNames;
}
