package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by MAY on 26/5/2016.
 */
@Getter
@Setter
@ToString
public class Fragment {
    private String id;
    private String type;
    private String name;
    private String covered;
    private String message;
    private String interactionOperator;
    private int indexFm;

}