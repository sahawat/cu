package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by MAY on 26/5/2016.
 */
@Getter
@Setter
@ToString
public class OwnedMemberFragment {
    private String id;
    private String name;
    private String type;
}
