package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by MAY on 30/5/2016.
 */
@Getter
@Setter
@ToString
public class SwitchModel {

    private String id;
    private String covered;
    private String message;
    private String type;
    private String value;
    private String receiveId;
    private String sendId;

}
