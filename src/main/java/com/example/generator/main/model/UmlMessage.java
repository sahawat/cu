package com.example.generator.main.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by MAY on 25/5/2016.
 */
@Getter
@Setter
@ToString
public class UmlMessage {

    private String id;
    private String type;
    private String name;
    private String receiveEvent;
    private String sendEvent;
    private String signaturename;
}
