package com.example.generator.main.service;

import com.example.generator.main.common.XMITag;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MAY on 16/5/2016.
 */
public class UserHandler extends DefaultHandler{

    WriteTag tag = new WriteTag();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        //Lifeline
        String idLF = "";
        String nameLF = "";
        String coverLF = "";

        //UmlMessage
        String idMsg = "";
        String nameMsg = "";
        String receiveMSg = "";
        String sendMsg = "";

        //Alternative
        String idFM = "";
        String interOperaFm = "";
        String nameFm = "";

        //Alternative.OwnedMember
        String idOM = "";
        String nameOM = "";
        String typeOM = "";

        //1
        if (qName.equalsIgnoreCase(XMITag.Model)) {
            //System.out.println("<"+XMITag.Model+">");

        //2
        } else if(qName.equalsIgnoreCase(XMITag.LIFELINE)) {
            idLF = attributes.getValue("xmi:id");
            nameLF = attributes.getValue("name");

            if ((null != nameLF) && (null != idLF)) {
                if (nameLF.equals("Sale Agent") || (nameLF.equals("Actor"))) {
                    return;
                }
                tag.lifelineTag(idLF, nameLF);
            }

        //3
        } else if(qName.equalsIgnoreCase(XMITag.COVEREDBY)){
            coverLF = attributes.getValue("xmi:idref");

            if(null != coverLF){
                tag.coverLFTag(StringUtils.trim(coverLF));
            }

        //4
        } else if(qName.equalsIgnoreCase(XMITag.MESSAGE)){
            idMsg = attributes.getValue("xmi:id");
            nameMsg = attributes.getValue("name");
            receiveMSg = attributes.getValue("receiveEvent");
            sendMsg = attributes.getValue("sendEvent");

            if(null != idMsg){
                tag.msgTag(idMsg, nameMsg, receiveMSg, sendMsg);
            }

            HashMap<String,String> msgHM = new HashMap<String,String>();
            msgHM.put(idMsg, nameMsg);

            if(null != idMsg){
                for(Map.Entry m:msgHM.entrySet()){
                    System.out.println(m.getKey()+" "+m.getValue());
                }
            }

        //5
        } else if(qName.equalsIgnoreCase(XMITag.FRAGMENT)){
            idFM = attributes.getValue("xmi:id");
            interOperaFm = attributes.getValue("interactionOperator");
            nameFm = attributes.getValue("name");

            if((null != interOperaFm) && (interOperaFm.equals("alt"))){
                tag.fragmentTag(idFM, interOperaFm, nameFm);
            }

        //6
        } else if(qName.equalsIgnoreCase(XMITag.OWNEDMEMBER)){
            idOM = attributes.getValue("xmi:id");
            nameOM = attributes.getValue("name");
            typeOM = attributes.getValue("xmi:type");

            if(!typeOM.equals("uml:InteractionOperand")){
                tag.ownedMemberTag(idOM, nameOM, typeOM);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase(XMITag.Model)) {
        }
    }
}
