package com.example.generator.main.service;

import com.example.generator.main.model.AlternativeModel;
import com.example.generator.main.model.SwitchAttr;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by MAY on 18/5/2016.
 */
public class WriteJAVA {

    private static final String TAB = "\t";
    private static final String SPACE_BAR = " ";
    private static final String NEW_LINE = "\n";
    public static String produceClass(String name){
        return String.format("" +
                "public class %s {" +
                "\n\n\t"+
                "${attributes}"+
                "\n"+
                "${injector}"+
                "\n"+
                "\n ${method} \n}\n",name);
    }

    public static String produceMethod(String name,String returnName){
        if(StringUtils.isEmpty(name)){
            return "";
        }
        name = replaceMethodParams(name);
        StringBuilder temp = new StringBuilder();
        if(StringUtils.isEmpty(returnName)) {
            temp.append("\tpublic void %s {");
        }else {
            temp.append("\n\t public String %s {");
        }
        temp.append(NEW_LINE);
        temp.append("\t\t\t");
        temp.append("\t\t${logic}");
        temp.append("\n\t\t ${fragment}");
        temp.append("\n\n\t %s;");
        temp.append("\t\n\t} \n");
        return String.format(temp.toString(), name, returnName);
    }

    private static String replaceMethodParams(String methodName){
        String result = methodName.substring(methodName.indexOf("(") + 1, methodName.indexOf(")"));
        String names[] = result.split(",");
        String sum = "";
        if(!StringUtils.isEmpty(result)) {
            for (String x : names) {
                sum = sum + "String " + x + ",";
            }
            sum = sum.substring(0,sum.length()-1);

        }
        return methodName.replaceAll("\\(" + result + "\\)", "(" + sum + ")");
    }

    public static String produceCombinedLoop(String combinedName){
        System.out.println("produceCombinedLoop()");
        return String.format("\n%s { \n ${ifCombined} \n}", combinedName);
    }

    public static String produceIf(List<AlternativeModel> combinedName){
        StringBuilder builder = new StringBuilder();
        int loopIndex = 0;
        for(AlternativeModel model : combinedName){
            System.out.println("***** : "+model);
            String condition = model.getMsgCondition();
            if(loopIndex==0) {
                builder.append("\nif(" + condition + ") {\n");
                builder.append("\n${ifLogic}\n");
            }else{
                builder.append("else if(" + condition + ") {\n");
            }
            builder.append("\n}");
            loopIndex++;
        }
        return builder.toString();
    }

    public static String produceCombinedSwitch(List<SwitchAttr> combinedSwitch){

        SwitchAttr switchAttr = combinedSwitch.get(0);
        StringBuilder builder = new StringBuilder();
        builder.append(NEW_LINE);
        builder.append("String "+switchAttr.getReturnMessage().split(" ")[0]+" = \"\";");
        builder.append(NEW_LINE);
        builder.append(NEW_LINE);
        builder.append("switch(String input) {\n");
        for(SwitchAttr c : combinedSwitch){
            String condition = c.getCondition();
            String returnMsg = c.getReturnMessage();
            condition = condition.substring(condition.indexOf('"'), condition.indexOf('"')+3);
            builder.append("\tcase "+condition+" :\n");
            builder.append("\t\t"+returnMsg);
            builder.append("\nbreak;");
        }
        builder.append("}");
        return builder.toString();
    }

    public static String produceCreateObject(String className){
        if(StringUtils.isEmpty(className))return "";
        StringBuilder builder = new StringBuilder();
        builder.append(className);
        builder.append(SPACE_BAR);
        builder.append(StringUtils.uncapitalize(className));
        builder.append(SPACE_BAR);
        builder.append(" = new ");
        builder.append(className);
        builder.append("();");
        return builder.toString();
    }

    public static String produceCallMethod(String destinationService,String methodDestination,String returnMsg){
        if(StringUtils.isEmpty(methodDestination)|| StringUtils.isEmpty(returnMsg) )return "";
        StringBuilder builder = new StringBuilder();
        builder.append("String");
        builder.append(SPACE_BAR);
        builder.append(returnMsg.replace("return ",""));
        builder.append(SPACE_BAR);
        builder.append(" = ");
        builder.append(StringUtils.uncapitalize(destinationService));
        builder.append(".");
        builder.append(methodDestination);
        builder.append(";");
        return builder.toString();
    }
}
