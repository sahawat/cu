package com.example.generator.main.service;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by MAY on 18/5/2016.
 */
public class WriteTag {

    private static final String PRE = "\\$\\{";
    private static final String POST = "\\}";
    private String lifeLine = "";
    private String fullLifeline = "";
    private String fullMessage = "";
    private String fullFragment = "";

    private String produceLFTag(String idLF, String nameLF){
        return String.format("\t<lifeline>\n" +
                                "\t\t<id>%s</id>\n" +
                                "\t\t<name>%s</name>\n" +
                                "\t\t<coveredBy>${coverLF}<coveredBy>\n" +
                             "\t</lifeline>", idLF, nameLF);
    }

    private String produceMsgTag(String idMsg, String nameMsg, String receiveMSg, String sendMsg){
        return String.format("\t<message>\n" +
                                "\t\t<id>%s</id>\n" +
                                "\t\t<name>%s</name>\n" +
                                "\t\t<receiveEvent>%s</receiveEvent>\n" +
                                "\t\t<sendEvent>%s</sendEvent>\n" +
                             "\t</message>", idMsg, nameMsg, receiveMSg, sendMsg);
    }

    private String produceFmTag(String idFM, String interOperaFm, String nameFm){
        return String.format("\t<message>\n" +
                                "\t\t<id>%s</id>\n" +
                                "\t\t<name>%s</name>\n" +
                                "\t\t<interactionOperator>%s</interactionOperator>\n" +
                             "\t</message>", idFM, interOperaFm, nameFm);

    }

    private String produceClass(String name){
        return String.format("public class %s {\n ${method} \n}\n",name);
    }

    /*
    * Start Lifeline
    */
    public void lifelineTag(String idLF, String nameLF){
        lifeLine = produceLFTag(idLF, nameLF);
        //System.out.println(lifeLine);
    }

    public void coverLFTag(String coverLF){
        fullLifeline = replaceContentLF(lifeLine, "coverLF", coverLF);
    }

    private String replaceContentLF(String temp, String key, String value) {
        temp =  temp.replaceAll(PRE + key + POST, value);
        //System.out.println(temp);
        return temp;
    }
    /*
    * End Lifeline
    */

    /*
    * Start UmlMessage
    */
    public void msgTag(String idMsg, String nameMsg, String receiveMSg, String sendMsg){
        fullMessage = produceMsgTag(idMsg, nameMsg, receiveMSg, sendMsg);
        //System.out.println(produceMsgTag(idMsg, nameMsg, receiveMSg, sendMsg));
    }
    /*
    * End UmlMessage
    */

    /*
    * Start Fragment
    */
    public void fragmentTag(String idFM, String interOperaFm, String nameFm){
        fullFragment = produceFmTag(idFM, interOperaFm, nameFm);
        //System.out.println(produceFmTag(idFM, interOperaFm, nameFm));
    }
    /*
    * End Fragment
    */

    /*
    * Start Fragment
    */
    public void ownedMemberTag(String idFM, String interOperaFm, String nameFm){
        // = produceFmTag(idFM, interOperaFm, nameFm);
        //System.out.println(produceFmTag(idFM, interOperaFm, nameFm));
    }
    /*
    * End Fragment
    */
}