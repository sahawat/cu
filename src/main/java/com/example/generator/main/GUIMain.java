package com.example.generator.main;

import com.example.generator.MainProcess;
import com.sun.prism.paint.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by MAY on 9/6/2016.
 */
public class GUIMain extends JFrame {

    //Variables of GUI
    private JPanel rootPanel;
    private JPanel HeaderJPanel;
    private JPanel MiddelJPanel;
    private JPanel BottomJPanel;
    private JLabel lbHeader;

    private JLabel lbSDJsonPath;
    private JTextField txtJsonPath;
    private JButton btnBrowse;

    private JLabel lbGenPath;
    private JTextField txtGenPath;
    private JButton btnLoCode;

    private JLabel lbCodeGen;
    private JProgressBar progressBar;
    private JButton btnGenCode;

    private JButton btnOpGenCode;
    private JButton btnGenCodeTable;
    private JButton btnViewGen;
    private JButton btnExit;
    private JButton btnClear;
    private JLabel lbCDJsonPath;
    private JTextField txtCDJsonPath;
    private JButton btnBrowseCD;
    private JButton testButton;


    //Other
    private File jsonFile = null;
    private File classCDFile = null;
    private String locationPath = null;
    OpenFIle openFIle = new OpenFIle();
    MainProcess mainProcess = new MainProcess();
    static final int MY_MINIMUM = 0;
    static final int MY_MAXIMUM = 100;

    //Start ProgressBar
    private boolean running = true;
    private Thread progressTask = new Thread();
    private int percent;

    private void initProgressBar() {
        progressBar.setMinimum(MY_MINIMUM);
        progressBar.setMaximum(MY_MAXIMUM);
        progressBar.setStringPainted(true);
        progressTask = new Thread() {
            public void run() {
                while (running) {
                    try {
                        progressBar.setValue(percent++);
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                progressBar.setValue(100);
            }
        };
    }
    //End ProgressBar

    public GUIMain() {
        super("UML JAVA CODE GENERATOR");

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().add(rootPanel); // or whatever...
        this.pack();
        this.setLocationRelativeTo(null);  // *** This page of center***
        this.setVisible(true);

        initProgressBar();
        disableBtnAndTxt();

        //Default btn BG
        Color colorDefault = Color.decode("#18413E");

        btnBrowse.addActionListener(e -> {
            jsonFile = openFIle.getFile();
            //System.out.println("jsonFile: "+jsonFile.toString());
            String ext = FilenameUtils.getExtension(jsonFile.getAbsolutePath());
            System.out.println(ext);
            if (ext.equals("json")) {
                txtJsonPath.setText(jsonFile.getAbsolutePath());
                txtJsonPath.setEnabled(true);
                btnLoCode.setEnabled(true);
                btnLoCode.setBackground(colorDefault);
            } else {
                showAlertMessage("This file must be only json.");
            }
        });

        btnBrowseCD.addActionListener(e -> {
            classCDFile = openFIle.getFile();
            //System.out.println("jsonFile: "+jsonFile.toString());
            String ext = FilenameUtils.getExtension(classCDFile.getAbsolutePath());
            System.out.println(ext);
            if (ext.equals("json")) {
                txtCDJsonPath.setText(classCDFile.getAbsolutePath());
                txtCDJsonPath.setEnabled(true);
                btnLoCode.setEnabled(true);
                btnLoCode.setBackground(colorDefault);
            } else {
                showAlertMessage("This file must be only json.");
            }
        });

        btnLoCode.addActionListener(e -> {
            locationPath = openFIle.getPathLocation();
            txtGenPath.setText(locationPath);
            txtGenPath.setEnabled(true);
            btnGenCode.setEnabled(true);
            btnGenCode.setBackground(colorDefault);
        });

        btnGenCode.addActionListener(e -> {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Would you like to generate code?", "Confirm", JOptionPane.YES_NO_OPTION);
            System.out.println(dialogResult);

            if (dialogResult == JOptionPane.YES_OPTION) {

                if (jsonFile != null && txtCDJsonPath != null && !StringUtils.isEmpty(locationPath)) {
                    initProgressBar();
                    progressBarStart();
                    try {
                        mainProcess.generateCode(jsonFile, classCDFile, txtGenPath.getText());
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    //openExplorer(locationPath);
                    progressBarStop();
                    btnOpGenCode.setEnabled(true);
                    btnGenCodeTable.setEnabled(true);
                    btnViewGen.setEnabled(true);
                    btnGenCode.setEnabled(false);
                    btnOpGenCode.setBackground(colorDefault);
                    btnGenCodeTable.setBackground(colorDefault);
                    btnViewGen.setBackground(colorDefault);
                    btnGenCode.setBackground(Color.DARK_GRAY);
                } else {
                    showAlertMessage("Please make sure your location path and FileJson");
                }
            }
        });

        btnOpGenCode.addActionListener(e -> {
            if (!StringUtils.isEmpty(locationPath)) {
                openExplorer(locationPath);
                btnOpGenCode.setEnabled(true);
                btnGenCodeTable.setEnabled(true);
                btnViewGen.setEnabled(true);
            }
        });

        btnGenCodeTable.addActionListener(e -> {
            GUITableGen guiTableGen = new GUITableGen(mainProcess.getProcessResult());
        });

        btnViewGen.addActionListener(e -> {
            GUIViewGen textViewer = new GUIViewGen(locationPath, mainProcess.getProcessResult().getFileNames());
        });

        btnClear.addActionListener(e -> {
            disableBtnAndTxt();
            txtJsonPath.setText("");
            txtGenPath.setText("");
        });

        btnExit.addActionListener(e -> System.exit(0));
        //btnExit.addActionListener(e-> new GUITableGen(mainProcess.getProcessResult()));
    }

    private void disableBtnAndTxt() {
        //Disabled btn
        btnLoCode.setEnabled(false);
        btnGenCode.setEnabled(false);
        btnOpGenCode.setEnabled(false);
        btnGenCodeTable.setEnabled(false);
        btnViewGen.setEnabled(false);
        txtJsonPath.setEnabled(false);
        txtGenPath.setEnabled(false);
        txtCDJsonPath.setEnabled(false);

        //Change btn BG
        btnLoCode.setBackground(Color.DARK_GRAY);
        btnGenCode.setBackground(Color.DARK_GRAY);
        btnOpGenCode.setBackground(Color.DARK_GRAY);
        btnGenCodeTable.setBackground(Color.DARK_GRAY);
        btnViewGen.setBackground(Color.DARK_GRAY);

        //Progress 0%
        progressBar.setValue(0);
    }

    private void progressBarStop() {
        running = false;
    }

    private void progressBarStart() {
        progressTask.start();
    }

    private void openExplorer(String path) {
        try {
            Desktop.getDesktop().open(new File(path));
        } catch (IOException e1) {
            showAlertMessage(e1.getMessage());
        }
    }

    private void showAlertMessage(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Alert", JOptionPane.OK_OPTION);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
