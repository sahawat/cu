package com.example.generator.main.common;

/**
 * Created by MAY on 11/5/2016.
 */
public class XMITag {

    public static String Model = "uml:Model";
    public static String LIFELINE = "lifeline";
    public static String MESSAGE = "message";
    public static String COVEREDBY = "coveredBy";
    public static String SIGNATURE = "signature";
    public static String FRAGMENT = "fragment";
    public static String OWNEDMEMBER = "ownedMember";

}
