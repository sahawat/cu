package com.example.generator.main;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.List;

/**
 * Created by MAY on 27/6/2016.
 */
public class GUIViewGen extends JFrame{
    private JPanel rootPanel;
    private JPanel MiddlePanel;
    private JList codeList;
    private JTextArea txtArGenCode;
    private JLabel lbClass;
    private JLabel lbGenCode;
    private JLabel lbViewGen;

    private String destinationPath;

    public GUIViewGen(String destinationPath ,List<String> files) {

        super("UML JAVA CODE GENERATOR");
        this.destinationPath = destinationPath;
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setPreferredSize(new Dimension(950, 825));
        this.getContentPane().add(rootPanel); // or whatever...
        this.pack();
        this.setLocationRelativeTo(null);  // *** This page of center***
        this.setVisible(true);

        codeList.setListData(files.toArray());

        codeList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                String file = codeList.getSelectedValue().toString();
                txtArGenCode.setFont(new Font("Cordia New (Body CS)", Font.PLAIN, 14));
                txtArGenCode.setText(readFile(file));
            }
        });
    }

    private String readFile(String filename){
        StringBuilder content = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(destinationPath+"/"+filename+".java"),"UTF8"));
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                content.append(sCurrentLine);
                content.append("\n");
            }
            System.out.println("content :: "+content.toString());

        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();

    }
}
