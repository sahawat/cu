package com.example.generator.main;

import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import java.io.File;

/**
 * Created by MAY on 6/11/2016.
 */
public class OpenFIle {

    JFileChooser fileChooser = new JFileChooser();

    public File getFile(){
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            System.out.println(file.getAbsolutePath());
            return file;
        }
        return null;
    }

    public String getPathLocation(){
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
            File file = fileChooser.getSelectedFile();
            return file.getAbsolutePath();
        }
        return "";
    }
}
