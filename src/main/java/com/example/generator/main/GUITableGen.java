package com.example.generator.main;

import com.example.generator.main.model.ProcessResult;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.*;
import java.awt.*;
import java.util.List;

/**
 * Created by MAY on 27/6/2016.
 */
public class GUITableGen extends JFrame {
    private JPanel rootPanel;
    private JLabel lbTableGenCode;
    private JPanel MiddlePanel;
    private JTable table1;
    private JFrame frame;


    public GUITableGen(ProcessResult processResult) {

        super("UML JAVA CODE GENERATOR");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setPreferredSize(new Dimension(950, 825));
        this.getContentPane().add(rootPanel); // or whatever...
        this.pack();
        this.setLocationRelativeTo(null);  // *** This page of center***
        this.setVisible(true);

        //Default btn BG
        Color colorDefault = Color.decode("#18413E");


        //create table with data
        MiddlePanel.setLayout(new BoxLayout(MiddlePanel, BoxLayout.Y_AXIS));

        String[] columnNames = { "Name", "Value"};


        Object[][] data = {
                { "Class", processResult.getClassSum()},
                { "Method", processResult.getMethodSum()},
                { "Condition and Loop", processResult.getConditionSum()}
        };
        table1 =  new TableBackroundPaint0(data, columnNames);
        //table1 = new JTable(data, columnNames);
        table1.getTableHeader().setFont(new Font("Century", Font.BOLD, 20));
        table1.getTableHeader().setForeground(colorDefault);
        table1.setForeground(colorDefault);
        table1.setFont(new Font("Century", Font.PLAIN, 18));
        table1.setRowHeight(30);

        MiddlePanel.add(table1.getTableHeader(), BorderLayout.PAGE_START);
        MiddlePanel.add(table1);
    }

    class TableBackroundPaint0 extends JTable {

        private static final long serialVersionUID = 1L;

        TableBackroundPaint0(Object[][] data, Object[] head) {
            super(data, head);
            setOpaque(false);
            ((JComponent) getDefaultRenderer(Object.class)).setOpaque(false);
        }

        @Override
        public void paintComponent(Graphics g) {
            Color background = new Color(168, 210, 241);
            Color controlColor = new Color(230, 240, 230);
            int width = getWidth();
            int height = getHeight();
            Graphics2D g2 = (Graphics2D) g;
            Paint oldPaint = g2.getPaint();
            g2.setPaint(new GradientPaint(0, 0, background, width, 0, controlColor));
            g2.fillRect(0, 0, width, height);
            g2.setPaint(oldPaint);
            for (int row : getSelectedRows()) {
                Rectangle start = getCellRect(row, 0, true);
                Rectangle end = getCellRect(row, getColumnCount() - 1, true);
                g2.setPaint(new GradientPaint(start.x, 0, controlColor, (int) ((end.x + end.width - start.x) * 1.25), 0, Color.orange));
                g2.fillRect(start.x, start.y, end.x + end.width - start.x, start.height);
            }
            super.paintComponent(g);
        }
    }
}
