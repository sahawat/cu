package com.example.generator;

import com.example.generator.main.model.*;
import com.example.generator.main.service.WriteJAVA;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by MAY on 24/5/2016.
 */
public class MainProcess {
    private static List<UmlMessage> listOfUmlMessage = new ArrayList<>();
    private static List<Fragment> LIST_OF_FRAGMENT = new ArrayList<>();
    private static List<SwitchModel> LIST_OF_SWITCH = new ArrayList<>();
    private static List<AlternativeModel> LIST_OF_ALTERNATIVE = new ArrayList<>();
    private static Map<String, String> combinedMap = new HashMap<String, String>();
    private static final String ACTOR_NAME = "Sale Agent";
    private static List<FragmentLifeLine> listFragLL = new ArrayList<>();
    private static Map<String, List<Attribute>> attributeMap;
    ProcessResult processResult = new ProcessResult();
    ClassDiagramGenerator classDiagramGenerator = new ClassDiagramGenerator();
    int methodSum;
    int classSum;
    int conditionSum;

    private List<String> fileNames = new ArrayList<>();

    public ProcessResult getProcessResult() {
        return processResult;
    }

    public void generateCode(File jsonFile,File classFile, final String destinationPath) throws ParseException, IOException {

        clearData();
        methodSum = 0;
        classSum = 0;
        conditionSum = 0;
        attributeMap = classDiagramGenerator.getAttributeMap(classFile);

        if (!jsonFile.exists() || !new File(destinationPath).isDirectory()) {

            //System.out.println("error");
        }

        WriteFileUtility writeFileUtility = new WriteFileUtility(destinationPath);


        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(jsonFile));
            JSONObject jsonObject = (JSONObject) obj;

            jsonObject = (JSONObject) jsonObject.get("xmi:XMI");
            JSONArray model = (JSONArray) jsonObject.get("uml:Model");
            model = getArrayFromJsonArray(model, "ownedMember");

            final Iterator[] i = {model.iterator()};

            // take each value from the json array separately
            JSONArray ownedBehaviorArr = null;
            while (i[0].hasNext()) {
                JSONObject innerObj = (JSONObject) i[0].next();
                if (innerObj.get("ownedBehavior") != null) {
                    ownedBehaviorArr = (JSONArray) innerObj.get("ownedBehavior");
                }
            }

            JSONObject ownedBehaviorObj = (JSONObject) ownedBehaviorArr.get(ownedBehaviorArr.size() - 1);

            //System.out.println("*************************************  Start Fragment  ********************************************");
            JSONArray fragmentArray = (JSONArray) ownedBehaviorObj.get("fragment");
            //System.out.println("fragmentArray :: "+fragmentArray);

            List<JSONObject> fragmentIden = getObjectsFromJsonArray(fragmentArray);

            final int[] indexFm = {0};
            fragmentIden.stream().forEach(fragmentChild -> {
                JSONObject fragmentJson = getObjectFromJsonObject(fragmentChild, "$");
                Fragment fragment = new Fragment();
                fragment.setId((String) fragmentJson.get("xmi:id"));
                fragment.setType((String) fragmentJson.get("xmi:type"));
                fragment.setName((String) fragmentJson.get("name"));
                fragment.setCovered((String) fragmentJson.get("covered"));
                fragment.setMessage((String) fragmentJson.get("message"));
                fragment.setInteractionOperator((String) fragmentJson.get("interactionOperator"));
                fragment.setIndexFm(indexFm[0]++);
                LIST_OF_FRAGMENT.add(fragment);

            });
            //LIST_OF_FRAGMENT.forEach(x -> System.out.println(x.toString()));

            List<Fragment> combinedFm = LIST_OF_FRAGMENT
                    .parallelStream()
                    .filter(data -> data.getInteractionOperator() != null)
                    .collect(Collectors.toList());

            for (Fragment x : combinedFm) {

                if (x.getInteractionOperator().equals("loop")) {

                    JSONObject loopObj = (JSONObject) fragmentArray.get(x.getIndexFm());
                    //1
                    JSONArray ownMemFmLoopArr = getArrayFromJsonObject(loopObj, "ownedMember");

                    //getCombinedLoop
                    List<JSONObject> ownMemListObj = getObjectsFromJsonArray(ownMemFmLoopArr);


                    ownMemListObj.stream().forEach(ownFmChild -> {
                        JSONArray coveredFmArr = getArrayFromJsonObject(ownFmChild, "covered");
                        JSONObject coveredFmObj = getObjectFromJsonArray(coveredFmArr, "$");
                        String idCoverFm = (String) coveredFmObj.get("xmi:idref");

                        JSONArray guardFmArr = getArrayFromJsonObject(ownFmChild, "guard");
                        JSONArray specificationObj = getArrayFromJsonArray(guardFmArr, "specification");
                        JSONObject idrefSpecObj = getObjectFromJsonArray(specificationObj, "$");
                        String idSpecValueFm = (String) idrefSpecObj.get("value");

                        //fragment Loop
                        JSONArray fragmentArr = getArrayFromJsonObject(ownFmChild, "fragment");
                        List<JSONObject> fragmentObj = getObjectsFromJsonArray(fragmentArr);
                        fragmentObj.stream().forEach(fragmentChild -> {
                            // System.out.println();
                            JSONObject fragmentArrObj = getObjectFromJsonObject(fragmentChild, "$");
                            String eventId = (String) fragmentArrObj.get("xmi:id");
                            String lifeLine = (String) fragmentArrObj.get("covered");
                            String message = (String) fragmentArrObj.get("message");

                            FragmentLifeLine fragmentLifeLine = new FragmentLifeLine();
                            fragmentLifeLine.setEventId(eventId);
                            fragmentLifeLine.setLifeLine(lifeLine);
                            fragmentLifeLine.setMessage(message);
                            listFragLL.add(fragmentLifeLine);
                        });


                        combinedMap.put(idCoverFm, idSpecValueFm);

                        //getCombinedSwitch
                        //2
                        //ได้ 1 ตัว
                        JSONArray ownedFmSwitchArr = getArrayFromJsonObject(ownFmChild, "ownedMember");

                        //3
                        // มี 4 ตัว
                        JSONArray eachSwitchArr = getArrayFromJsonArray(ownedFmSwitchArr, "ownedMember");

                        ////****** Start Level เดียวกัน ******////
                        List<JSONObject> switchObj = getObjectsFromJsonArray(eachSwitchArr);

                        List<String> valueSpecList = new ArrayList<>();

                        // วนค่า 4 ตัว
                        switchObj.stream().forEach(switchChild -> {
                            // level 1
                            JSONObject startSwitchObj = getObjectFromJsonObject(switchChild, "$");
                            String nameSwitchStr = startSwitchObj.get("name").toString();
                            String valueSpecStr = "";
                            if (nameSwitchStr.startsWith("switch")) {
                                JSONArray guardEachSwitchArr = getArrayFromJsonObject(switchChild, "guard");
                                JSONArray specSwitchArr = getArrayFromJsonArray(guardEachSwitchArr, "specification");
                                JSONObject specSwitchObj = getObjectFromJsonArray(specSwitchArr, "$");
                                valueSpecStr = specSwitchObj.get("value").toString();
                                valueSpecList.add(valueSpecStr);
                            }

                            //fragment send,receive (method, return)
                            // level 1
                            JSONArray fragmentSwitchArr = getArrayFromJsonObject(switchChild, "fragment");
                            List<JSONObject> fragmentSwitchObj = getObjectsFromJsonArray(fragmentSwitchArr);

                            JSONArray covered = getArrayFromJsonObject(switchChild, "covered");
                            for (JSONObject xx : fragmentSwitchObj) {
                                SwitchModel switchModel = new SwitchModel();
                                JSONObject fmOwnedSwitch = getObjectFromJsonObject(xx, "$");
                                switchModel.setId(fmOwnedSwitch.get("xmi:id").toString());
                                switchModel.setType(fmOwnedSwitch.get("xmi:type").toString());
                                switchModel.setCovered(fmOwnedSwitch.get("covered").toString());
                                switchModel.setMessage(fmOwnedSwitch.get("message").toString());
                                switchModel.setValue(valueSpecStr);

                                //sendId
                                JSONObject jsonObject1 = (JSONObject) covered.get(1);
                                JSONObject xxx = getObjectFromJsonObject(jsonObject1, "$");
                                String sendId = (String) xxx.get("xmi:idref");
                                switchModel.setReceiveId(sendId);

                                //receiveId
                                JSONObject jsonObject2 = (JSONObject) covered.get(0);
                                JSONObject xxx2 = getObjectFromJsonObject(jsonObject2, "$");
                                String sendId2 = (String) xxx2.get("xmi:idref");
                                switchModel.setSendId(sendId2);

                                LIST_OF_SWITCH.add(switchModel);
                            }

                        });
                        ////****** End Level เดียวกัน ******////
                    });

                } else if (x.getInteractionOperator().equals("alt")) {
                    JSONObject altObj = (JSONObject) fragmentArray.get(x.getIndexFm());
                    JSONArray ownMemFmAltArr = getArrayFromJsonObject(altObj, "ownedMember");
                    List<JSONObject> ownMemAltListObj = getObjectsFromJsonArray(ownMemFmAltArr);

                    for (JSONObject ownFmChild : ownMemAltListObj) {
                        AlternativeModel alter = new AlternativeModel();
                        JSONArray coveredFmArr = getArrayFromJsonObject(ownFmChild, "covered");
                        // GET SENDER RECEIVE
                        JSONObject coveredFmObj = getObjectFromJsonObject((JSONObject) coveredFmArr.get(0), "$");
                        alter.setSendId(coveredFmObj.get("xmi:idref").toString());

                        coveredFmObj = getObjectFromJsonObject((JSONObject) coveredFmArr.get(1), "$");
                        alter.setReceiveId(coveredFmObj.get("xmi:idref").toString());

                        //GET CONDITION
                        JSONArray guardFmArr = getArrayFromJsonObject(ownFmChild, "guard");
                        JSONArray specificationObj = getArrayFromJsonArray(guardFmArr, "specification");
                        JSONObject nameSpecObj = getObjectFromJsonArray(specificationObj, "$");
                        String idSpecValueFm = (String) nameSpecObj.get("value");
                        alter.setMsgCondition(idSpecValueFm);
                        LIST_OF_ALTERNATIVE.add(alter);

                        //covered By fragmentAltArr
                        JSONArray fragmentAltArr = getArrayFromJsonObject(ownFmChild, "fragment");
                        List<JSONObject> fragmentAltObj = getObjectsFromJsonArray(fragmentAltArr);
                        //System.out.println("------------- fragmentAltObj "+fragmentAltObj);
                        fragmentAltObj.stream().forEach(fragmentAltChild -> {
                            JSONObject fragmentAltArrObj = getObjectFromJsonObject(fragmentAltChild, "$");
                            //System.out.println("fragmentAltArrObj :: "+fragmentAltArrObj);
                            String eventId = (String) fragmentAltArrObj.get("xmi:id");
                            String lifeLine = (String) fragmentAltArrObj.get("covered");
                            String message = (String) fragmentAltArrObj.get("message");

                            FragmentLifeLine fragmentLifeLine = new FragmentLifeLine();
                            fragmentLifeLine.setEventId(eventId);
                            fragmentLifeLine.setLifeLine(lifeLine);
                            fragmentLifeLine.setMessage(message);
                            listFragLL.add(fragmentLifeLine);
//                            }
                        });
                        //listFragLL.forEach(op -> System.out.println(op.toString()));
                    }
                }
            }

            //System.out.println("*******************************  End FRAGment  ****************************************************");
            //System.out.println("***************************************  Start UmlMessage  **************************************************");

            JSONArray messageArray = (JSONArray) ownedBehaviorObj.get("message");
            List<JSONObject> messageIden = getObjectsFromJsonArray(messageArray);

            messageIden.stream().forEach(lifeLineChild -> {
                JSONObject messageJson = getObjectFromJsonObject(lifeLineChild, "$");
                UmlMessage msg = new UmlMessage();
                msg.setId((String) messageJson.get("xmi:id"));
                msg.setType((String) messageJson.get("xmi:type"));
                msg.setReceiveEvent((String) messageJson.get("receiveEvent"));
                msg.setSendEvent((String) messageJson.get("sendEvent"));
                msg.setName((String) messageJson.get("name"));

                JSONArray signatureMsgArr = getArrayFromJsonObject(lifeLineChild, "signature");
                JSONObject returnSigObj = getObjectFromJsonArray(signatureMsgArr, "$");
                msg.setSignaturename((String) returnSigObj.get("name"));
                listOfUmlMessage.add(msg);

            });

            //listOfUmlMessage.forEach(x -> System.out.println(x.toString()));

            //System.out.println("***************************************  End UmlMessage  **************************************************");

            //System.out.println();
            //System.out.println("***************************************  Start Lifeline  **************************************************");

            JSONArray lifeLineArr = (JSONArray) ownedBehaviorObj.get("lifeline");
            List<JSONObject> lifeLineIden = getObjectsFromJsonArray(lifeLineArr);


            //convert LifeLine
            List<Lifeline> lifelineList = new ArrayList<>();
            for (JSONObject lifeLineChild : lifeLineIden) {
                JSONObject lifeLineObj = getObjectFromJsonObject(lifeLineChild, "$");
                Lifeline lifeline = new Lifeline();
                String idClass = (String) lifeLineObj.get("xmi:id");
                String className = (String) lifeLineObj.get("name");
                lifeline.setId(idClass);
                lifeline.setName(className);
                JSONArray coveredBy = getArrayFromJsonObject(lifeLineChild, "coveredBy");
                List<String> listOfEvent = new ArrayList<>();
                // System.out.println(listFragLL.stream().filter(data -> data.getLifeLine().equals(idClass)).collect(Collectors.toList()).size());

                List<FragmentLifeLine> listOfFragment = listFragLL.stream().filter(data -> data.getLifeLine().equals(idClass)).collect(Collectors.toList());

                listOfEvent.addAll(listOfFragment.stream().map(FragmentLifeLine::getEventId).collect(Collectors.toList()));
                if (coveredBy != null) {
                    List<JSONObject> receiveEvent = getObjectsFromJsonArray(coveredBy);
                    for (JSONObject receive : receiveEvent) {
                        JSONObject o1 = getObjectFromJsonObject(receive, "$");
                        String id = (String) o1.get("xmi:idref");
                        listOfEvent.add(id);
                    }
                }
                lifeline.setListOfEvent(listOfEvent);
                if ((lifeLineObj.get("name")).equals(ACTOR_NAME)) {
                    continue;
                }
                lifelineList.add(lifeline);
            }

            listOfUmlMessage.forEach(System.out::println);
            for (Lifeline lifeline : lifelineList) {
                //System.out.println("lifeline : " + lifeline.toString());
                String idClass = lifeline.getId();
                String className = lifeline.getName();

                String classTemp = produceClassTemp(className);
                String methodTemp = "";
                String loopTemp = "";
                String logicTemp = "";
                final List<String> listOfEvent = lifeline.getListOfEvent();
                String loopName = combinedMap.get(idClass);
//                System.out.println("idClass : "+idClass);
                // System.out.println("className : " + className);

                //switch case
                List<SwitchModel> switchModelList = LIST_OF_SWITCH.parallelStream().filter(data -> data.getCovered().equals(idClass)).collect(Collectors.toList());
                List<SwitchAttr> switchAttrs = new ArrayList<>();

                if (!switchModelList.isEmpty()) {
                    String value = "";
                    SwitchAttr switchAttr = new SwitchAttr();
                    for (SwitchModel switchCase : switchModelList) {
                        Optional<UmlMessage> msg = listOfUmlMessage.parallelStream().filter(data -> data.getId().equals(switchCase.getMessage())).findFirst();
                        if (msg.isPresent()) {
                            if (switchCase.getReceiveId().equals(idClass)) {
                                String msgName = convertUnicodeToThai(msg.get().getName());

                                if (!switchCase.getValue().equals(value)) {
                                    switchAttr = new SwitchAttr();
                                    switchAttr.setCondition(switchCase.getValue());
                                    switchAttrs.add(switchAttr);
                                }

                                if (msgName.startsWith("return")) {
                                    switchAttr.setReturnAttr(msgName);

                                } else {
                                    switchAttr.setReturnMessage(msgName);
                                }
                                value = switchCase.getValue();
                            }
                        }
                    }
                    conditionSum++;
                }


                /**
                 *  START : get relation class for creating the object
                 */

                UmlMessage callReturnMethod = null;
                UmlMessage callMethod = null;
                //System.out.println("listOfEvent : "+listOfEvent.size());
                for (String eventId : listOfEvent) {
                    if (callMethod == null) {
                        callMethod = getCallMethodName(eventId);
                    }
                    if (callReturnMethod == null) {
                        callReturnMethod = getReturnMethodName(eventId);
                    }
                }

                // System.out.println("callMethod : " + callMethod);
                //System.out.println("callReturnMethod : " + callReturnMethod);
                //Find Destination ClassModel
                String nextServiceName = "";
                if (callMethod != null) {
                    boolean isNextService = false;
                    for (Lifeline lif : lifelineList) {
                        for (String eventId : lif.getListOfEvent()) {
                            if (isNextService) break;
                            if (callMethod.getReceiveEvent().equals(eventId)) {
                                nextServiceName = lif.getName();
                                isNextService = true;
                                break;
                            }
                        }

                    }
                }
                /**
                 *  END : get relation class for creating the object
                 */


                //create method
                if (!listOfEvent.isEmpty()) {
                    //listOfEvent.forEach(System.out::println);
                    UmlMessage methodObj = getThisMethodName(listOfEvent);
                    if (methodObj != null) {
                        String returnName = getMethodReturn(listOfEvent);
                        String methodName = methodObj.getName();
                        methodTemp = produceMethodTemp(methodName, returnName);
                    }
                }


                if (loopName != null) {
                    //produce loop
                    loopTemp = produceLoopTemp(loopName);
                }

                if ((switchAttrs != null) && (!switchAttrs.isEmpty())) {
                    logicTemp = produceSwitchTemp(switchAttrs);
                }

                boolean isIf = false;
                List<AlternativeModel> listOfAlt = LIST_OF_ALTERNATIVE.parallelStream().filter(data -> data.getSendId().equals(idClass)).collect(Collectors.toList());
                if (!listOfAlt.isEmpty()) {
                    logicTemp = produceIfTemp(listOfAlt);
                    if (!StringUtils.isEmpty(loopName)) {
                        String returnName = getMethodReturn(listOfEvent);
                        logicTemp = replaceContent(logicTemp, "ifLogic", produceCallMethod(nextServiceName, getMessageName(callMethod, ""), returnName));

                        logicTemp = replaceContent(loopTemp, "ifCombined", logicTemp);
                    }
                    isIf = true;
                    conditionSum++;
                }
                methodTemp = replaceContent(methodTemp, "logic", logicTemp);
                if (!StringUtils.isEmpty(methodTemp)) {
                    methodSum++;
                }
                classSum++;
                String returnName = getMethodReturn(listOfEvent);
                methodTemp = replaceContent(methodTemp, "fragment", isIf ? "" : produceCallMethod(nextServiceName, getMessageName(callMethod, ""), returnName));

                classTemp = replaceContent(classTemp, "method", methodTemp);

                StringBuilder attributeText = new StringBuilder();
                if (attributeMap.containsKey(className)) {
                    for (Attribute attribute : attributeMap.get(className)) {
                        attributeText.append(attribute.getModifier() + " " + attribute.getType() + " " + attribute.getName() + ";");
                        attributeText.append("\n");
                    }
                }
                classTemp = replaceContent(classTemp, "attributes", attributeText.toString());
                classTemp = replaceContent(classTemp, "injector", produceCreateObj(nextServiceName));

                String filePath = writeFileUtility.writeJavaFile(className, classTemp);
                fileNames.add(filePath);
            }

            processResult.setFileNames(fileNames);
            processResult.setClassSum(classSum);
            processResult.setMethodSum(methodSum);
            processResult.setConditionSum(conditionSum);

            //System.out.println("***************************************  End Lifeline  **************************************************");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static String getMessageName(UmlMessage umlMessage, String defaultVal) {
        if (umlMessage == null) return defaultVal;
        return umlMessage.getName();
    }


    public static String convertUnicodeToThai(String convertLangStr) {
        return StringEscapeUtils.unescapeJava(convertLangStr);
    }

    private static UmlMessage getThisMethodName(List<String> eventList) {
        for (String id : eventList) {
            if (getCurrentMethodName(id) != null) {
                return getCurrentMethodName(id);
            }
        }
        return null;
    }

    private static String getMethodReturn(List<String> eventList) {
        for (String eventId : eventList) {
            if (!StringUtils.isEmpty(getMethodReturnNameBySigName(eventId))) {
                return getMethodReturnNameBySigName(eventId);
            }
        }
        return "";
    }

    private static UmlMessage getMethodNameBySigName(String eventId) {
        Optional<UmlMessage> thisClassMsg = listOfUmlMessage
                .parallelStream()
                .filter(data -> data.getSignaturename() == null)
                .filter(data -> data.getReceiveEvent().equals(eventId))
                .findFirst();
        if (thisClassMsg.isPresent()) {
            return thisClassMsg.get();
        }
        return new UmlMessage();
    }

    private static UmlMessage getCurrentMethodName(String eventId) {
        Optional<UmlMessage> thisClassMsg = listOfUmlMessage
                .parallelStream()
                .filter(data -> data.getSignaturename() == null)
                .filter(data -> data.getReceiveEvent().equals(eventId))
                .findFirst();
        if (thisClassMsg.isPresent()) {
            return thisClassMsg.get();
        }
        return null;
    }

    private static UmlMessage getCallMethodName(String eventId) {
        //System.out.println("getCallMethodNamegetCallMethodNamegetCallMethodName   :   "+eventId );
        Optional<UmlMessage> thisClassMsg = listOfUmlMessage
                .parallelStream()
                .filter(data -> data.getSignaturename() == null)
                .filter(data -> data.getSendEvent().equals(eventId))
                .findFirst();
        if (thisClassMsg.isPresent()) {
            return thisClassMsg.get();
        }
        return null;
    }


    private static UmlMessage getReturnMethodName(String eventId) {
        Optional<UmlMessage> thisClassMsg = listOfUmlMessage
                .parallelStream()
                .filter(data -> data.getSignaturename() != null && data.getSignaturename().equalsIgnoreCase("return"))
                .filter(data -> data.getReceiveEvent().equals(eventId))
                .findFirst();
        if (thisClassMsg.isPresent()) {
            return thisClassMsg.get();
        }
        return null;
    }


    private static String getMethodReturnNameBySigName(String eventId) {
        Optional<UmlMessage> thisClassMsg = listOfUmlMessage
                .parallelStream()
                .filter(data -> data.getSignaturename() != null && data.getSignaturename().equalsIgnoreCase("return"))
                .filter(data -> data.getSendEvent().equals(eventId))
                .findFirst();
        if (thisClassMsg.isPresent()) {
            return thisClassMsg.get().getName();
        }
        return "";
    }


    private static final String PRE = "\\$\\{";
    private static final String POST = "\\}";

    private static String replaceContent(String temp, String key, String value) {
//        try {
        temp = temp.replaceAll(PRE + key + POST, value);
//        }catch (IllegalArgumentException ex){
//            System.out.println(ex.getMessage());
//        }
        return temp;
    }

    private static String produceClassTemp(String className) {
        return WriteJAVA.produceClass(className);
    }

    private static String produceMethodTemp(String methodName, String returnName) {
        return WriteJAVA.produceMethod(methodName, returnName);
    }

    private static String produceIfTemp(List<AlternativeModel> list) {
        return WriteJAVA.produceIf(list);
    }


    private static String produceLoopTemp(String loopName) {
        return WriteJAVA.produceCombinedLoop(loopName);
    }

    private static String produceSwitchTemp(List<SwitchAttr> switchList) {
        return WriteJAVA.produceCombinedSwitch(switchList);
    }

    private static String produceCreateObj(String className) {
        return WriteJAVA.produceCreateObject(className);
    }

    private static String produceCallMethod(String destinationService, String destinationMethod, String returnMsg) {
        return WriteJAVA.produceCallMethod(destinationService, destinationMethod, returnMsg);
    }

    private static void printListOfJson(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.size(); i++) {
            // System.out.println("The " + i + " element of the array: " + jsonArray.get(i));
        }
    }

    private static void printListOfJson(JSONObject jsonObject) {
        // jsonObject.forEach((x, y) -> System.out.println("key : " + x + " , value : " + y));
    }

    public static JSONObject getObjectFromJsonObject(JSONObject jsonObject, String key) {
        return (JSONObject) jsonObject.get(key);
    }

    public static List<JSONObject> getObjectsFromJsonArray(final JSONArray array) {
        List<JSONObject> list = new ArrayList<>();
        JSONArray jsonArray = array;
        if (jsonArray == null) {
            return list;
        }
        JSONObject obj;
        for (Object x : jsonArray) {
            obj = (JSONObject) x;
            list.add(obj);
        }
        return list;
    }

    public static JSONArray getArrayFromJsonArray(JSONArray jsonArray, String key) {
        JSONObject obj = (JSONObject) jsonArray.get(0);
        return (JSONArray) obj.get(key);
    }

    public static JSONObject getObjectFromJsonArray(JSONArray jsonArray, String key) {
        JSONObject obj = (JSONObject) jsonArray.get(0);
        return (JSONObject) obj.get(key);
    }

    public static JSONArray getArrayFromJsonObject(JSONObject jsonObject, String key) {
        return (JSONArray) jsonObject.get(key);
    }

    private void clearData() {
        listOfUmlMessage.clear();
        LIST_OF_FRAGMENT.clear();
        LIST_OF_SWITCH.clear();
        LIST_OF_ALTERNATIVE.clear();
        combinedMap.clear();
        listFragLL.clear();
        fileNames.clear();
    }

    private void clearDat() {
        listOfUmlMessage.clear();
        LIST_OF_FRAGMENT.clear();
        LIST_OF_SWITCH.clear();
        LIST_OF_ALTERNATIVE.clear();
        combinedMap.clear();
        listFragLL.clear();
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////